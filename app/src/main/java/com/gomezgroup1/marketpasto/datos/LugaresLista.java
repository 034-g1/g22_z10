package com.gomezgroup1.marketpasto.datos;

import com.gomezgroup1.marketpasto.modelo.Lugar;
import com.gomezgroup1.marketpasto.modelo.TipoLugar;

import java.util.ArrayList;
import java.util.List;

//Creamos una clase que utiliza la interface RepositorioLugares
public class LugaresLista implements RepositorioLugares {

    protected List<Lugar> listaLugares; //Creamos una lista que almacena objetos de tipo lugar
//Constructor
    public LugaresLista() {
        listaLugares = new ArrayList<Lugar>();
        añadeEjemplos();
    }
    //Vamos a buscar un elemento a partir de la posicion en la lista
    @Override
    public Lugar elemento(int id) {
        return listaLugares.get(id);
    }
    //Vamos agregar un objeto de lugar
    @Override
    public void añade(Lugar lugar) {
        listaLugares.add(lugar);
    }
    //Creamos un objeto de lugar y agregamos en la ultima posición
    @Override
    public int nuevo() {
        Lugar lugar = new Lugar();
        listaLugares.add(lugar);
        return listaLugares.size()-1;
    }
    //Borramos un objeto de la lista
    @Override
    public void borrar(int id) {
        listaLugares.remove(id);
    }
    //Sirve para reccorrer la lista con el tamaño
    @Override
    public int tamaño() {
        return listaLugares.size();
    }
    //Actualizamos un objeto de la lista
    @Override
    public void actualiza(int id, Lugar lugar) {
        listaLugares.set(id,lugar);
    }
    public void añadeEjemplos() {

        añade(new Lugar("GROUP GOMEZ JP INTERNATIONAL COP","Centro de Negocios NOVA CENTER",
                -75.59013,6.256864, TipoLugar.EDUCACION, 0,"http://GroupGomezJp.com/",
                "Departamento de Nariño - Pasto",  5));
        añade(new Lugar("BUTIQ AVELINO 100% CUERO","Bombona - Nova Center",
                -74.07695,4.64888, TipoLugar.COMPRAS, 5470183,
                "https://AvelinoCuero.com/","Excelente calidad, excelente atención",5));
        añade(new Lugar("Transporte One Minute","Unico - Pasto", -72.90982,
                7.2466845, TipoLugar.OTROS,0, "SIN DATOS",
                "Lugar entre los departamentos Santander y Norte de Santander", 4));
        añade(new Lugar("Market Pasto","Av. Los Estudiantes", 0,
                0, TipoLugar.BAR, 0, "SIN DATOS",
                "Lugar en Pasto - Nariño",4));

    }

}
