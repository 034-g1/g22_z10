package com.gomezgroup1.marketpasto.presentacion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.gomezgroup1.marketpasto.R;

public class PreferenciasFragment extends PreferenceFragment {
    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias); }




}
