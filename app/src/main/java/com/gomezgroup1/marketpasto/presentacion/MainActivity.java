package com.gomezgroup1.marketpasto.presentacion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.gomezgroup1.marketpasto.Aplicacion;
import com.gomezgroup1.marketpasto.R;
import com.gomezgroup1.marketpasto.casos_usos.CasosUsoLugar;
import com.gomezgroup1.marketpasto.casos_usos.CasosUsosActividades;
import com.gomezgroup1.marketpasto.datos.LugaresLista;
import com.gomezgroup1.marketpasto.datos.RepositorioLugares;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {


    private RepositorioLugares lugares;
    private CasosUsoLugar usoLugar;
    private CasosUsosActividades usosActividades;
    static final int RESULTADO_PREFERENCIAS = 0;
    private RecyclerView recyclerView;
    public AdaptadorLugares adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this,lugares);
        usosActividades= new CasosUsosActividades(this);

        adaptador =((Aplicacion) getApplication()).adaptador;
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptador);
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicion = recyclerView.getChildAdapterPosition(v);
                usoLugar.mostrar(posicion);

            }
        });


        //barra de acciones
        Toolbar toolbar = findViewById(R.id.toolbar_Main);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout_Main);
        toolbar.setTitle(getTitle());
        //Boton flotante FAB circular
        /*
        FloatingActionButton fab = findViewById(R.id.btnFlotante);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.mensaje_fab, Snackbar.LENGTH_LONG).setAction("Accion",null).show();
            }
        });*/


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.ajustes){
            usosActividades.lanzarPreferencias(RESULTADO_PREFERENCIAS);

            return true;
        }
        if (id == R.id.acercaDe){
            usosActividades.lanzarAcercaDe();
            return true;
        }
        if (id == R.id.menu_buscar){
            lanzarVistaLugar(null);
            Log.d("Tag main","clic a la opcion buscar");
            return true;
        }
        if (id == R.id.menu_usuario){
            lanzarUsuario();
            return true;
        }
        if (id == R.id.menu_mapa){
            Toast.makeText(getApplicationContext(),"No disponible",Toast.LENGTH_SHORT).show();
            return true;
        }
        if (id == R.id.Historial_Compras){
            Toast.makeText(getApplicationContext(),"Sin historial",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //abrir acerca de
    public void lanzarAcercade(View view){
        Intent abrir = new Intent(this, AcercaDeActivity.class);
        startActivity(abrir);
    }

    public void lanzarVistaLugar(View view){
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle(R.string.elijaLugar)
                .setMessage("indica su id:")
                .setView(entrada)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                int id = Integer.parseInt(entrada.getText().toString());
                                usoLugar.mostrar(id);
                            }})
                .setNegativeButton("Cancelar", null)
                .show();
    }


    public void lanzarUsuario(){ Intent abrirUsuario = new Intent (this,
            UsuarioActivity.class);
        startActivity(abrirUsuario); }


}