package com.gomezgroup1.marketpasto.presentacion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gomezgroup1.marketpasto.R;
import com.gomezgroup1.marketpasto.datos.BaseDatosCel;
import com.gomezgroup1.marketpasto.presentacion.MainActivity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class InicioSesion extends AppCompatActivity{

    Button iniciarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);

        iniciarSesion = findViewById(R.id.btniniciar);

        iniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Inició correctamente",Toast.LENGTH_SHORT).show();

                // FUNCIONA LA BD
                //Guarda la bd en android
                BaseDatosCel BaseDatosCel = new BaseDatosCel(InicioSesion.this,"pastobd",null,1);

                //Crea la base en sqlite

                SQLiteDatabase db1 = BaseDatosCel.getWritableDatabase();
                
                if(db1!=null){
                    Toast.makeText(getApplicationContext(),"Base de datos creada",Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(getApplicationContext(),"Error creando la base de datos",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}