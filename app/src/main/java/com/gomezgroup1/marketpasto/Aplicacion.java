package com.gomezgroup1.marketpasto;

import android.app.Application;

import com.gomezgroup1.marketpasto.datos.LugaresLista;
import com.gomezgroup1.marketpasto.datos.RepositorioLugares;
import com.gomezgroup1.marketpasto.presentacion.AdaptadorLugares;

public class Aplicacion extends Application {


    public RepositorioLugares lugares = new LugaresLista();

    public AdaptadorLugares adaptador = new AdaptadorLugares(lugares);


    @Override
    public void onCreate() {
        super.onCreate();
    }

    //get de repositoriolugares
    public RepositorioLugares getLugares() {
        return lugares;
    }
}

