package com.gomezgroup1.marketpasto.casos_usos;

import android.app.Activity;
import android.content.Intent;

import com.gomezgroup1.marketpasto.presentacion.AcercaDeActivity;
import com.gomezgroup1.marketpasto.presentacion.PreferenciasActivity;

public class CasosUsosActividades {

    protected Activity actividad;

    //constructor de la clase

    public CasosUsosActividades(Activity actividad) {
        this.actividad = actividad;
    }

    public void lanzarAcercaDe() {
        actividad.startActivity(new Intent(actividad, AcercaDeActivity.class
        ));}
    public void lanzarPreferencias(int codidoSolicitud) {
        actividad.startActivityForResult(new Intent(actividad, PreferenciasActivity.class), codidoSolicitud); }
}
